import { atom } from "recoil";

export const currentBlogEntry = atom({
    key: 'currentBlogEntry',
    default: {
        title: 'Test',
        content: `This is an example blog`
    }
});