import { selector } from "recoil";
import { currentBlogEntry } from '../atoms/blog';

export const currentBlogTitle = selector({
    key: 'currentBlogTitle',
    get: ({get}) => {
        return get(currentBlogEntry).title;
    },
    set: ({set, get}, newTitle) => {
        set(currentBlogEntry, {...get(currentBlogEntry), title: newTitle});
    }
});

export const currentBlogContent = selector({
    key: 'currentBlogContent',
    get: ({get}) => {
        return get(currentBlogEntry).content;
    },
    set: ({set, get}, newContent) => {
        set(currentBlogEntry, {...get(currentBlogEntry), content: newContent});
    }
});