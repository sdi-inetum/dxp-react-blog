import React from 'react';

import blogStyles from './Blog.module.css'
import Button from '../../partials/Button/Button';
import TitleText from '../../partials/TitleText/TitleText';
import InputTextBox from '../../partials/InputTextBox/InputTextBox';
import InputTextArea from '../../partials/InputTextArea/InputTextArea';

function Blog() {
    return (
        <div className={blogStyles.blog}>
            <TitleText>My Blog</TitleText>

            <InputTextBox {...{ onChange: (event) => console.log(`Title Changed: ${event.target.value}`) }}/>
            <InputTextArea {...{ onChange: (event) => console.log(`Content changed: ${event.target.value}`)}}/>

            <Button onClick={() => console.log(`Hey, who touched me?`)} label={`Submit`}/>
        </div>
    );
}

export default Blog;



