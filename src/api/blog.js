import CONSTANTS from '../../constants';

const { URL } = CONSTANTS;

export const postBlogEntryToBackend = (blogEntry) => {
    return fetch(`${URL.HOST.BLOG_BACKEND}/api/articles`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(blogEntry)
    })
        .then(() => {

        })
        .catch((error) => {

        });
};
