export default {
    URL: {
        HOST: {
            BLOG_BACKEND: `http://localhost:1234`
        }
    },
    ACTIONS: {
        BLOG: {
            BLOG_ADDED_RESET: `BLOG_ADDED_RESET`,
            BLOG_ADDED_SUCCESS: `BLOG_ADDED_SUCCESS`,
            BLOG_ADDED_FAILED: `BLOG_ADDED_FAILED`
        }
    },
    STATUS: {
        IDLE: `IDLE`,
        ERROR: `ERROR`,
        SUCCESS: `SUCCESS`,
        PENDING: `PENDING`
    },
    DEFAULT_BLOG_VALUES: {
        NAME: `SEPPE`,
        EMAIL: `myemail@gmail.com`,
        DESCRIPTION: `Example blog title`
    }
}