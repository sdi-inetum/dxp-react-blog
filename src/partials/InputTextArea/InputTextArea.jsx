import React from 'react';

const InputTextArea = ({placeHolderText, onChange}) => {
    return <textarea className={`blog-input`} cols={ `40` } rows={`5`} defaultValue={placeHolderText} onChange={onChange} data-testid={`blog-input`}/>
};

InputTextArea.defaultProps = {
    placeHolderText: "Enter your blog here",
    onChange: () => {}
};

export default InputTextArea