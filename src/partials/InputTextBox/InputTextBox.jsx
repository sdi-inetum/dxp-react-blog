import React from 'react';

const InputTextBox = ({placeHolderText, onChange}) => {
    return <input className={`blog-input`} type={`text`} onChange={onChange} placeholder={placeHolderText} />
};

InputTextBox.defaultProps = {
    placeHolderText: "Enter your title"
};

export default InputTextBox