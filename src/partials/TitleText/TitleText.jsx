import React from 'react';

const TitleText = ({children}) => {
    return <h1>{children}</h1>;
};

export default TitleText;