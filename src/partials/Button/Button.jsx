import React from 'react';

import buttonStyles from './Button.module.css'

const Button = ({ label, onClick }) => {
    return <button className={buttonStyles.btn} onClick={onClick}>{label}</button>;
};

export default Button